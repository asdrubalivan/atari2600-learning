; 2600 para dummies

    processor 6502
    include "vcs.h"
    include "macro.h"

PATTERN = $80
TIMETOCHANGE = 20

    SEG
    ORG $F000

Reset
    ; Clear Ram
    ldx #0
    lda #0
Clear
    sta 0,x
    inx
    bne Clear

    ; Inicialización de una sola vez
    lda #0
    sta PATTERN

    lda #$45
    STA COLUPF

    ldy #0

StartOfFrame
    
    ; Comenzamos con los VBLANK
    lda #0
    sta VBLANK

    lda #2
    sta VSYNC

    sta WSYNC
    sta WSYNC
    sta WSYNC

    lda #0
    sta VSYNC

    ; 37 scanlines de vertical blank
    ldx #0
VerticalBlank
    sta WSYNC
    inx
    cpx #37
    bne VerticalBlank

; Fin de VerticalBlank
    iny ; Incrementamos velocidad 
    cpy #TIMETOCHANGE ; Ha llegado a nuestro punto de cambio?
    bne notyet ; Si no, vamos a notyet
    ldy #0
    inc PATTERN

notyet
    lda PATTERN ; usamos nuestro pattern
    sta PF1     ; como nuestra forma de playfield

    ldx #0 ; Cuenta nuestro numero de scanlines
Picture
    stx COLUBK  ; Cambiamos el color del fondo
    sta WSYNC   ; Esperamos hasta el final del scanline
    inx
    cpx #192
    bne Picture

    lda #%01000010
    sta VBLANK
    ldx #0
 
Overscan sta WSYNC
    inx
    cpx #30
    bne Overscan

    jmp StartOfFrame

    ORG $FFFA

InterruptVectors
    .word Reset     ; NMI
    .word Reset     ; RESET
    .word Reset     ; IRQ


    END 
