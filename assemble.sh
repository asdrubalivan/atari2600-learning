# ${1} sera el nombre del asm
# ${2} sera el nombre del bin
EJECUTABLE="./dasm.Linux.x86"
DIR="bin/"
INCLUDEDIR="include/"
LISTINGDIR="listings/"
if [[ ! -d ${DIR} ]]; then
    mkdir ${DIR}
fi
if [[ ! -d ${LISTINGDIR} ]]; then
    mkdir ${LISTINGDIR}
fi

${EJECUTABLE} ${1} -o${DIR}${2}.bin -f3 -v4 -I${INCLUDEDIR} -l${LISTINGDIR}${2}.txt 
