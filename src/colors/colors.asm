    processor 6502
    include "vcs.h"
    include "macro.h"

    SEG
    ORG $F000

Reset
StartOfFrame
    
; Vertical blanks

    lda #0
    sta VBLANK

    lda #2
    sta VSYNC

; Scanlines of VSYNCH
    sta WSYNC
    sta WSYNC
    sta WSYNC

    lda #0
    sta VSYNC

    ; 37 scanlines de WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC

    ;192 scanlines de dibujo

    ldx #0
    REPEAT 192
        inx
        inx
        stx COLUBK
        sta WSYNC
    REPEND

    lda #%01000010
    sta VBLANK ; Fin de la pantalla comience el blanqueo

    ; 30 scanlines de overscan

    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC
    sta WSYNC

    jmp StartOfFrame

    ORG $FFFA

    .word Reset ; NMI
    .word Reset ; RESET
    .word Reset ; IRQ
    END
